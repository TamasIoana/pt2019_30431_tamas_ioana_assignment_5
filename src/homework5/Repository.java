package homework5;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;



public class Repository 
{
	public ArrayList<MonitoredData> data;
	
	public Repository() 
	{
		data = new ArrayList<>();
		getData();
	}
	
	public void getData() 
	{
		File file = new File("src/activities.txt"); // buffer
		BufferedReader reader;
		try 
		{
			reader = new BufferedReader(new FileReader(file));
			//line will be a line of text from the activities.txt
			String line;
			//we will save each field from a line, the block of text separated by tabs from the rest
			String[] fields;
			//read until there are no lines left 
			while ((line = reader.readLine()) != null) 
			{
				fields = line.trim().split("\\t+");//remove spaces and split by tabs
				//add data in the list of data
				data.add(new MonitoredData(fields[0],fields[1],fields[2]));
			}
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("File not found");
		} 
		catch( IOException e) 
		{
			System.out.println("File not found");
		}
	 }
	

}
