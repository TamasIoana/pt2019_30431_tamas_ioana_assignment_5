package homework5;


public class Main{
	
	public static void main(String[] args) 
	
	{
		
		Controller c = new Controller(new Repository());
		c.genStatistic1();
		c.genStatistics2();
		c.genStatistics3();
		c.genStatistic5();
		System.out.println("Task 1 -> " + c.statistic0);
		System.out.println("Task 2 ->" + c.statistic1.toString());
		System.out.println("Task 3 ->" + c.statistic2.toString());
        System.out.println("Task 5 ->" + c.statistic5.toString());
	}

}