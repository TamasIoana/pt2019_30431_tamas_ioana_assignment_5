package homework5;

public class MonitoredData
{
		private String startTime;
		private String endTime;
		private String activity;
		
		public MonitoredData(String startTime, String endTime, String activity)
		{
			this.setStartTime(startTime);
			this.setEndTime(endTime);
			this.setActivity(activity);
		}	
		
	

		public String getEndTime() 
		{
			return endTime;
		}

		public void setEndTime(String endTime) 
		{
			this.endTime = endTime;
		}

		public String getStartTime() 
		{
			return startTime;
		}

		public void setStartTime(String startTime) 
		{
			this.startTime = startTime;
		}

		public String getActivity()
		{
			return activity;
		}

		public void setActivity(String activity) 
		{
			this.activity = activity;
		}
		
		
		
		public String toString() 
		{
			return "Start Time: " + getStartTime().toString() + " End Time: " + getEndTime().toString() + " Activity: " + getActivity() + "\n";
		}
		
}